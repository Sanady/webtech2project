<!DOCTYPE html>
<html lang="sk">
     <head>
        <title>Main Page</title>
         <meta charset="utf-8">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
         <link rel="stylesheet" type="text/css" href="style.css"> 
    </head>
    <body>
        <ul>
            <li><a class="active" href="#home">Home</a></li>
            <li><a href="odhlasenie.php">Logout</a></li>
        </ul>
<?php
require "config.php";

$conn = new mysqli($db_servername, $db_username, $db_password, $db_name);

if (!$conn) {
	die("Connection failed:".mysqli_connect_error());
}

$sql = "SELECT id, login, cas_prihlasenia, typ FROM prihlasenia";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo "<table class='table'>
    <thead>
    <tr>
        <th scope='col'>ID</th>
        <th scope='col'>Username</th>
        <th scope='col'>Cas prihlasenia</th>
        <th scope='col'>Typ</th>
    </tr>
    </thead>
    <tbody>";
    while($row = $result->fetch_assoc()) {
        echo "<tr><th scope='row'>" . $row["id"]. "</th><td>" . $row["login"]. "</td><td>" . $row["cas_prihlasenia"]. "</td><td>" .$row["typ"]."</td></tr>";
    }
    echo "</tbody></table>";
} else {
    echo "0 results";
}
$conn->close();

?>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
</html>