<!DOCTYPE html>
<html>
<head>
	<title></title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

</body>

</html>
<?php
function get_client_ip()
{
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if (getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if (getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if (getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if (getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if (getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
$ip = get_client_ip(); // the IP address to query
//test ip
//$ip = "95.180.67.153"; // Beograd, Rakovica
$query = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
if ($query && $query['status'] == 'success')
{
    require "./config.php";
    $conn = new mysqli($servername, $username, $password, $dbname);
    $conn->set_charset("utf8");

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    //get Coords
    $lat = $query['lat'];
    //echo $lat;
    $lon = $query['lon'];
    //echo $lon;

    $mesto = $query['city'];
    $krajina = $query['country'];
    $datum = date("Y-m-d");
    $navstevaCas = date('H');
    if($navstevaCas >=6 && $navstevaCas<=14){
        $navstevaCas = "6:00 - 14:00";
    } elseif ($navstevaCas > 14 && $navstevaCas <= 20){
        $navstevaCas = "14:00 - 20:00";
    } elseif ($navstevaCas > 20 && $navstevaCas <= 24){
        $navstevaCas = "20:00 - 24:00";
    } elseif ($navstevaCas > 20 && $navstevaCas < 6){
        $navstevaCas = "20:00 - 6:00";
    }
    echo "Mesto: ".$mesto."<br>";
    echo "Krajina: ".$krajina."<br>";

    $query2 = file_get_contents('https://restcountries.eu/rest/v2/name/' . $krajina);
    $info = json_decode($query2, true);
    $hlavne = $info[0][capital];
    $vlajka = $info[0][flag];

    $sql = "INSERT INTO adresy(ip,krajina, mesto, den, vlajka, navstevaCas, ktora) VALUES ('$ip','$krajina','$mesto','$datum','$vlajka', '$navstevaCas', 'pocasie.php')";
    //echo $sql;
    $result = $conn->query($sql);

    $sql = "SELECT krajina,vlajka, COUNT(*) AS vysledok FROM adresy GROUP BY krajina,vlajka";
    $result = $conn->query($sql);
    //echo $sql;

    $url ="http://api.openweathermap.org/data/2.5/weather?lat=" . $lat . "&lon=" . $lon. "&appid=9d905d743b12ce428c087a50ecb98d54";
    //echo $url;

    $djson = file_get_contents($url);
    //echo $djson;
    $djson = substr_replace($djson, "[", 0, 0);
    $djson = substr_replace($djson, "]", strlen($djson), 0);
    //echo $djson;
    $info = json_decode($djson,true);
    $teplota = $info[0][main][temp] - 273.15;
    $minimalnaTeplota = $info[0][main][temp_min] - 273.15;
    $maximalnaTeplota = $info[0][main][temp_max] - 273.15;
    //$teplota = (5*($farnheit-32))/9;
    echo "Teplota: ".$teplota."C<br>";
    echo "Minimálna teplota: ".$minimalnaTeplota."°C<br>";
    echo "Maximálna teplota: ".$maximalnaTeplota."°C<br>";
    echo "Počasie: ".$info[0][weather][0][main];
}
else
{
    echo "nepodarilo sa získať údaje";
}
 ?>