<!DOCTYPE html>
<html>
<head>
    <title>zadanie 7</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

</body>
</html>

<?php

function get_client_ip()
{
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if (getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if (getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if (getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if (getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if (getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
$ip = get_client_ip(); // the IP address to query
//test ip
//$ip = "95.180.67.153"; // Beograd, Rakovica
//$ip = "54.65.108.119";
$query = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
if ($query && $query['status'] == 'success') {
    require "./config.php";
    $conn = new mysqli($servername, $username, $password, $dbname);
    $conn->set_charset("utf8");

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }


    //get Coords
    $lat = $query['lat'];
    //echo $lat;
    $lon = $query['lon'];
    //echo $lon;

    $mesto = $query['city'];
    $krajina = $query['country'];
    $datum = date("Y-m-d");
    $cas = date('H');
    //echo $cas;
    if($cas >=6 && $cas<=14){
        $cas = "6:00 - 14:00";
    } elseif ($cas > 14 && $cas <= 20){
        $cas = "14:00 - 20:00";
    } elseif ($cas > 20 && $cas <= 24){
        $cas = "20:00 - 24:00";
    } elseif ($cas > 24 && $cas < 6){
        $cas = "24:00 - 6:00";
    }

    $query2 = file_get_contents('https://restcountries.eu/rest/v2/name/' . $krajina);
    $info = json_decode($query2, true);
    $hlavne = $info[0][capital];
    $vlajka = $info[0][flag];

     $sql = "INSERT INTO adresy(ip,krajina, mesto, den, vlajka, navstevaCas, ktora) VALUES ('$ip','$krajina','$mesto','$datum','$vlajka', '$navstevaCas', 'statistika.php')";
    //echo $sql;
    $result = $conn->query($sql);

    $sql = "SELECT krajina,vlajka, COUNT(*) AS vysledok FROM adresy GROUP BY krajina,vlajka";
    $result = $conn->query($sql);

    
    echo "<table>
                <tr>
                    <th class='th1'>Vlajka</th>
                    <th class='th1'>Krajina</th>
                    <th class='th1'>Pocet</th>
                </tr>";

    while ($row = mysqli_fetch_assoc($result)) {

        echo "
                <tr>
                    <td><img src=\"" . $row['vlajka'] . "\" alt=\"vlajka\" height=\"42\" width=\"42\"></td>
                    <td><a href='http://147.175.121.210:8118/z7/statistika.php?zobraz=" . $row['krajina'] . "'>" . $row['krajina'] . "</a></td>
                    <td>" . $row['vysledok'] . "</td>
                </tr>";
    }
    echo "</table>";

    $sql = "SELECT COUNT(CASE WHEN navstevaCas = '6:00 - 14:00' THEN 1
                  ELSE NULL
             END) AS a
       ,COUNT(CASE WHEN navstevaCas = '14:00 - 20:00' THEN 1
                   ELSE NULL
              END) AS b
              ,COUNT(CASE WHEN navstevaCas = '20:00 - 24:00' THEN 1
                   ELSE NULL
              END) AS c
       ,COUNT(CASE WHEN navstevaCas = '24:00 - 6:00' THEN 1
                   ELSE NULL
              END) AS d
    FROM adresy";
    $result = $conn->query($sql);
    //echo $sql;
    echo "<table>
                <tr>
                    <th class='th3'>6:00 - 14:00</th>
                    <th class='th3'>14:00 - 20:00</th>
                    <th class='th3'>20:00 - 24:00</th>
                    <th class='th3'>24:00 - 6:00</th>
                </tr>";
    while ($row = mysqli_fetch_assoc($result)) {

        echo "
                <tr>
                    <td>" . $row['a'] . "</td>
                    <td>" . $row['b'] . "</td>
                    <td>" . $row['c'] . "</td>
                    <td>" . $row['d'] . "</td>
                </tr>";
    }
    echo "</table>";


    if (isset($_GET['zobraz'])) {
        $stat = htmlspecialchars($_GET['zobraz']);

        $sql = "SELECT mesto,krajina, COUNT(*) AS vysledok FROM adresy WHERE krajina = '$s' GROUP BY mesto";
        $result = $conn->query($sql);
        echo "<table>
                <tr>
                    <th class='th2'>Mesto</th>
                    <th class='th2'>Pocet</th>
                    <th class='th2'>Nelokalizované mestá a vidiek</th>
                </tr>";

        while ($row = mysqli_fetch_assoc($result)) {

            echo "
                <tr>
                    <td>" . $row['mesto'] . "</td>
                    <td>" . $row['vysledok'] . "</td>
                    <td>0</td>
                </tr>";
        }
        echo "</table>";
    }
    $sql = "SELECT ktora, COUNT(ktora) AS vysledok FROM adresy GROUP BY ktora ORDER BY vysledok DESC LIMIT 1";
    $result = $conn->query($sql);
    $row = mysqli_fetch_assoc($result);
    echo "najnavstevovanejsia stranka: ".$row['ktora'];
}
else
{
    echo "nepodarilo sa získať údaje";
}

?>