<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Painting</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script>

    <style>
    body{
      background-color: #c9c9c9;
    }
    #canvas {
      width: 100%;
      height: 600px;
      border: 1px solid black;
    }
    .jumbotron{
        background-color: #383836;
    }
    h1{
      color: #383836;
    }
    </style>

  </head>
  <body>
    <div class="container">
      <div class="jumbotron">
        <h1>Zadanie č.4</h1>
        <hr>
        <div class="well well-lg">
          Velkosť:
          <select class="" name="" id="thinckness">
            <option value="1">1</option>
            <option value="2" selected>2</option>
            <option value="5">5</option>
            <option value="10" >10</option>
            <option value="20">20</option>
            <option value="50">50</option>
          </select>
          <hr>
          Farba:
          <select class="" name="" id="color">
            <option value="#ffffff">Biela</option>
            <option value="#000000" >Čierna</option>
            <option value="#c60000">Červená</option>
            <option value="#36b201" selected>Zelená</option>
            <option value="#0153b2">Modrá</option>
            <option value="#e8d400">Žltá</option>
          </select>
          <a id="btn-Convert-Html2Image" class="btn btn-success btn-xs" href="#" style="float: right">Ulož obrázok</a>
        </div>
        <hr>
        <div id="canvas"></div>

      </div>
    </div>

    <script type="application/javascript">

    (function() {
      var socket = new WebSocket('ws://147.175.121.210:9118/Zadanie4');

      socket.onopen = function (message){
        console.log("Websocket started.");
      };

      socket.onerror = function (error) {
          console.log('WebSocket error: ' + error);
      };

      function createCanvas(parent, width, height) {
          var canvas = {};
          canvas.node = document.createElement('canvas');
          canvas.context = canvas.node.getContext('2d');
          canvas.node.width = width || 100;
          canvas.node.height = height || 100;
          parent.appendChild(canvas.node);
          return canvas;
      }

      function init(container, width, height, fillColor) {
          var canvas = createCanvas(container, width, height);
          var ctx = canvas.context;
          
          ctx.fillCircle = function(x, y, radius, fillColor) {
              this.fillStyle = fillColor;
              this.beginPath();
              this.moveTo(x, y);
              this.arc(x, y, radius, 0, Math.PI * 2, false);
              this.fill();
          };
          ctx.clearTo = function(fillColor) {
              ctx.fillStyle = fillColor;
              ctx.fillRect(0, 0, width, height);
          };
          ctx.clearTo(fillColor || "#ddd");

         
          canvas.node.onmousemove = function(e) {
              if (!canvas.isDrawing) {
                 return;
              }
              var color = document.getElementById('color').value;
              var x = e.pageX - this.offsetLeft;
              var y = e.pageY - this.offsetTop;
              var radius = document.getElementById('thinckness').value; 
              var fillColor = color;
              ctx.fillCircle(x, y, radius, fillColor);

              var data = {"x": x, "y": y, "radius": radius, "fillColor": color};
              socket.send(JSON.stringify(data));

              socket.onmessage = function (message) {
                  var data = JSON.parse(JSON.parse(message.data).utf8Data);
                  console.log(data);

                  ctx.fillCircle(data.x, data.y, data.radius, data.fillColor);
                 
              };
          };
          canvas.node.onmousedown = function(e) {
              canvas.isDrawing = true;
          };
          canvas.node.onmouseup = function(e) {
              canvas.isDrawing = false;
          };
      }
      var container = document.getElementById('canvas');
      init(container, container.offsetWidth, container.offsetHeight, '#ddd');


      var element = $("canvas"); 
      var getCanvas;
      $("#btn-Convert-Html2Image").on('click', function () {
        html2canvas(element, {
          onrendered: function (canvas) {
              getCanvas = canvas;
           }
       });
      var imgageData = getCanvas.toDataURL("image/png");
      var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
      $("#btn-Convert-Html2Image").attr("download", "picture.png").attr("href", newData);
      });

  })();

    </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </body>
</html>
