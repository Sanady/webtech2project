<?php
$target_dir = "/home/xrener/public_html/Zadania1/file/";
//$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;


$name=$_POST['meno'];
$imageFileType = strtolower(pathinfo($target_dir . basename($_FILES["fileToUpload"]["name"]),PATHINFO_EXTENSION));
$target_file=$target_dir . $name.".".$imageFileType;

// Check if image file is a actual image or fake image

// Check if file already exists
if (file_exists($target_file)) {
    $count = 1;
    $newtarget_file = $target_dir . "(".$count.")" . basename($_FILES["fileToUpload"]["name"]);
    rename($target_file, $newtarget_file);
    $count += 1; 
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
?>