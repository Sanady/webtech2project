// Pavol Marak
// 22.2.2019
//******************

// math constants
#define _USE_MATH_DEFINES
#define PI 3.14159265358979323846

// for Visual C++
#define _CRT_SECURE_NO_DEPRECATE

// arena
#define ARENA_W 70 // without border
#define ARENA_H 16 // without border

// symbols
#define P_MONSTER '+'
#define P_HUNTER 'H'

//posun
#define HUNTER_STEP 5
#define MONSTER_STEP 2
#define MONSTER_PROXIMITY 2

// macros for clearing console window and sleeping
#ifdef _WIN32
#include <windows.h>
#define CLEAR_SCREEN() system("cls")
#define SLEEP(sleepMs) Sleep(sleepMs)
#else
#include <unistd.h>
#define CLEAR_SCREEN() system("clear")
#define SLEEP(sleepMs) usleep(sleepMs * 1000)
#endif

//headers
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

// function declarations
void print_arena(float x_H, float y_H, float x_M, float y_M, int u_H, int u_M, float distance);

float get_new_x(float x_H, int u_H, int dist);
float get_new_y(float y_H, int u_H, int dist);
float distance_two_points(float x_H, float y_H, float x_M, float y_M);

int main()
{
	// hunter, monster
	float x_H, y_H;
	float x_M, y_M;
	int a_H = 0, a_M = 0;
	int option;
	int step = HUNTER_STEP;
	float distance;

	// seed rand
	srand(time(NULL));

	// placing the hunter randomly
	x_H = rand() % ARENA_W;
	y_H = rand() % ARENA_H;
	a_H = rand() % (360 + 1 - 1) + 1;

	x_M = rand() % ARENA_W;
	y_M = rand() % ARENA_H;
	a_M = rand() % (360 + 1 - 1) + 1;

	distance = distance_two_points(x_H, y_H, x_M, y_M);
	print_arena(x_H, y_H, x_M, y_M, a_H, a_M, distance);

	while (1)
	{
		if (distance < MONSTER_PROXIMITY) {
			printf("\n\nGAME OVER!\n");
			break;
		}

		printf("\n\nInput heading: (0) - forward, (1) - left, (2) - right, choose:\n");
		scanf("%d", &option);

		if (option == 1) {
			a_H = a_H + 30;
			x_H = get_new_x(x_H, a_H, step);
			y_H = get_new_y(y_H, a_H, step);
		}
		else if (option == 2){
			a_H = a_H - 30;
			x_H = get_new_x(x_H, a_H, step);
			y_H = get_new_y(y_H, a_H, step);
		}
		else if (option == 0) {
			a_H = a_H;
			x_H = get_new_x(x_H, a_H, step);
			y_H = get_new_y(y_H, a_H, step);
		}
		else {
			printf("\n\nWrong input, check again!\n");
		}

		//prechod cez steny
		if (y_H >= ARENA_H) {
			y_H = 1;
		}
		if (y_H <= 0) {
			y_H = 15;
		}
		if (x_H >= ARENA_W) {
			x_H = 1;
		}
		if (x_H <= 0) {
			x_H = 69;
		}

		if (y_M >= ARENA_H) {
			y_M = 1;
		}
		if (y_M <= 0) {
			y_M = 15;
		}
		if (x_M >= ARENA_W) {
			x_M = 1;
		}
		if (x_M <= 0) {
			x_M = 69;
		}

		distance = distance_two_points(x_H, y_H, x_M, y_M);
		print_arena(x_H, y_H, x_M, y_M, a_H, a_M, distance);

		a_M = rand() % (360 + 1 - 1) + 1;
		x_M = x_M + (cos(a_H * PI / 180) * MONSTER_STEP);
		y_M = y_M + (sin(a_H * PI / 180) * MONSTER_STEP);
	}
	return 0;
}



// function to print the contents of the arena
// IMPORTANT:
//   * use this function only for printing
//   * function does not print the character if it is located outside the arena
//   * you can modify this function
void print_arena(float x_H, float y_H, float x_M, float y_M, int u_H, int u_M, float distance)
{
	//CLEAR_SCREEN(); // clearing screen
	printf("ARENA SIZE: %dx%d\n", ARENA_W, ARENA_H);
	printf("(%c) Hunter: (%.2f,%.2f,%d)\n", P_HUNTER, x_H, y_H,u_H);
	printf("(%c) Monster: (%.2f,%.2f,%d)\n", P_MONSTER, x_M, y_M,u_M);
	printf("Vzdialenost: %.2f\n", distance);

	// inverting Y-axis for both hunter and monster
	y_H = ARENA_H - 1 - y_H;
	y_M = ARENA_H - 1 - y_M;

	int i, j, first_x, second_x;
	char first, second;
	for (i = 0; i < ARENA_H; i++)
	{
		// row contains both hunter and monster
		if (i == (int)(y_H) && i == (int)(y_M))
		{
			// hunter is closer to the left side
			if (x_H < x_M)
			{
				first = P_HUNTER;
				second = P_MONSTER;
				first_x = (int)x_H;
				second_x = (int)x_M;
			}
			// monster is closer to the left side
			else
			{
				first = P_MONSTER;
				second = P_HUNTER;
				first_x = (int)x_M;
				second_x = (int)x_H;
			}

			if (first_x < 0 && (second_x >= 0 && second_x < ARENA_W))
			{
				// printing spaces
				for (j = 0; j < second_x; j++)
				{
					printf(" ");
				}
				// printing second character
				printf("%c", second);
			}


			if ((first_x >= 0 && first_x < ARENA_W) && (second_x >= 0 && second_x < ARENA_W))
			{
				// printing spaces
				for (j = 0; j < first_x; j++)
				{
					printf(" ");
				}
				// printing first character
				printf("%c", first);
				// printing spaces
				for (j = 0; j < abs(second_x - first_x) - 1; j++)
				{
					printf(" ");
				}
				// printing second character
				printf("%c", second);

			}

			if ((first_x >= 0 && first_x < ARENA_W) && second_x >= ARENA_W)
			{
				// printing spaces
				for (j = 0; j < first_x; j++)
				{
					printf(" ");
				}
				// printing first character
				printf("%c", first);
			}

		}
		// row contains only hunter
		else if (i == (int)(y_H))
		{
			if ((int)x_H < ARENA_W && (int)x_H >= 0)
			{
				// printing spaces
				for (j = 0; j < x_H; j++)
				{
					printf(" ");
				}
				// printing the hunter
				printf("%c", P_HUNTER);
			}
		}
		// row contains only monster
		else if (i == (int)(y_M))
		{
			if ((int)x_M < ARENA_W && (int)x_M >= 0)
			{
				// printing spaces
				for (j = 0; j < x_M; j++)
				{
					printf(" ");
				}
				// printing the monster
				printf("%c", P_MONSTER);
			}
		}
		printf("\n");
	}
	//printf("Press ENTER to refresh the arena.");
	//getchar();
	//SLEEP(2000);
}

float get_new_x(float x_H, int a_H, int dist)
{
	float new_x = 0;
	new_x = x_H + (cos(a_H * PI / 180) * dist);
	return new_x;
}

float get_new_y(float y_H, int a_H, int dist)
{
	float new_y = 0;
	new_y = y_H + (sin(a_H * PI / 180) * dist);
	return new_y;
}

float distance_two_points(float x_H, float y_H, float x_M, float y_M)
{
	float vz;
	vz = sqrt(pow((x_M - x_H), 2) + pow((y_M - y_H), 2));
	return vz;
}
