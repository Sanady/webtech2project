<?php
include_once("config.php");

$conn = mysqli_connect($host, $user, $pass, $db);
mysqli_query($conn, "SET NAMES 'utf8';");

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
?>

<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Zadania 2</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
    <script src="main.js"></script>
</head>
<body>
    <div class="container">
    <h1 style="text-align:center;">Zadania 2:</h1>
    <?php 
    echo "<br><a href='newrecord.php' type='button' class='btn btn-primary btn-sm' >New Record</a><br><br>";

    $sql = "SELECT person.id_person, person.name, person.surname, OH.year, OH.country, person_placed.discipline
    FROM person, person_placed, OH
    WHERE person.id_person = person_placed.id_person AND OH.id_OH = person_placed.id_OH";
    $result = mysqli_query($conn, $sql);
    
    if (mysqli_num_rows($result) > 0) {
        echo "<table id='dbtable' class='table table-dark'><thead><tr>
        <th scope='col'>ID</th>
        <th scope='col'>Name</th>
        <th scope='col'>Surname</th>
        <th scope='col'>Year</th>
        <th scope='col'>Country</th>
        <th scope='col'>Discipline</th>
        <th scope='col'>Option</th></tr></thead><tbody>";
        while($row = mysqli_fetch_assoc($result)) {
            echo "<tr><th scope='row'>". $row["id_person"] . "</th>
            <td>" . $row["name"] . "</td>
            <td>" . $row["surname"] . "</td>
            <td>" .$row["year"]. "</td>
            <td>". $row["country"] . "</td>
            <td>" . $row["discipline"]. "</td>
            <td><button type='button' class='btn btn-danger btn-sm' onClick='deleteRow(".$row["id_person"].")'>Delete</button></td>
            <td><a href='edit.php?id=". $row["id_person"] ."' type='button' class='btn btn-success btn-sm' >Edit</a></td></tr>";
        }
        echo "</tbody></table>";
    } else {
        
    }
    echo "<br><h2>Top 10:</h2><br>";

    echo "<table class='table table-dark'>
        <tr>
            <th>Meno</th>
            <th>Priezvisko</th>
            <th>Zlate medaile</th>
        </tr>";
    $sql2 = "SELECT p.name, p.surname,COUNT(pp.place) AS NumberOfMedal
            FROM person AS p, person_placed AS pp
            WHERE p.id_person = pp.id_person AND pp.place = 1
            GROUP BY p.name, p.surname 
            ORDER BY `NumberOfMedal`  DESC 
            LIMIT 10;";
            $result2 = mysqli_query($conn, $sql2);

    if (mysqli_num_rows($result2) > 0) {
        while ($row = mysqli_fetch_assoc($result2)) {
            echo "<tr><td>".$row["name"]."</td>
            <td>".$row["surname"]."</td>
            <td>".$row["NumberOfMedal"]."</td></tr>";
        }
    }
        
    echo "</table>";
    ?>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>