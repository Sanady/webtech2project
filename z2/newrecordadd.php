<!DOCTYPE <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Insert Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
    <script src="main.js"></script>
</head>
<body>
<?php
include_once("config.php");

$conn = mysqli_connect($host, $user, $pass, $db);
mysqli_query($conn, "SET NAMES 'utf8';");

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
if(!isset($_GET["Name"]) && !isset($_GET["Surname"])){
    echo '<div class="container"><br><div class="alert alert-danger" role="alert">
    You haven`t entered key values!<br><br>
    <a href="newrecord.php" type="button" class="btn btn-danger">Back</a>
    </div></div>';
}
else{
    $fname = htmlspecialchars($_GET["Name"]);
    $lname = htmlspecialchars($_GET["Surname"]);
    $birth = htmlspecialchars($_GET["DayOfBirth"]);
    $bplace = htmlspecialchars($_GET["BirthPlace"]);
    $bcountry = htmlspecialchars($_GET["BirthCountry"]);
    $dday = htmlspecialchars($_GET["DeathDay"]);
    $dplace = htmlspecialchars($_GET["DeathPlace"]);
    $dcountry = htmlspecialchars($_GET["DeathCountry"]);

    $sql1 = "SELECT * FROM `person` WHERE `name` = '$fname' AND `surname` = '$lname'";
    $result = mysqli_query($conn, $sql1);

    if (mysqli_num_rows($result) > 0) {
        echo '<div class="container"><br><div class="alert alert-danger" role="alert">
        Record which you want to add already exists!<br><br>
        <a href="newrecord.php" type="button" class="btn btn-danger">Back</a>
        </div></div>';
    }
    else{
        $sql = "INSERT INTO `person` (`name`, `surname`, `birthDay`, `birthPlace`, `birthCountry`, `deathDay`, `deathPlace`, `deathCountry`) 
                            VALUE ('$fname', '$lname', '$birth', '$bplace', '$bcountry', '$dday', '$dplace', '$dcountry')";

        if (mysqli_query($conn, $sql)) {
            header('Location: index.php');
        } else {
            echo 'Error creating record: ' . mysqli_error($conn);
        }
    }
}


mysqli_close($conn);

?>
</body>
</html>
