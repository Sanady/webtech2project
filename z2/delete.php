<?php
include_once("config.php");

$conn = mysqli_connect($host, $user, $pass, $db);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$idOf = $_GET["id"];

$sql = "DELETE FROM person WHERE id_person=" . $idOf;
if (mysqli_query($conn, $sql)) {
    echo "Record deleted successfully";
    header("Location: index.php");
} else {
    echo "Error deleting record: " . mysqli_error($conn);
}

mysqli_close($conn);
?>