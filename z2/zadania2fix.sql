-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 06, 2019 at 09:24 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zadania2fix`
--

-- --------------------------------------------------------

--
-- Table structure for table `oh`
--

DROP TABLE IF EXISTS `oh`;
CREATE TABLE IF NOT EXISTS `oh` (
  `id_OH` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(3) NOT NULL,
  `year` int(4) NOT NULL,
  `order_OH` int(50) NOT NULL,
  `country` varchar(100) NOT NULL,
  PRIMARY KEY (`id_OH`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oh`
--

INSERT INTO `oh` (`id_OH`, `type`, `year`, `order_OH`, `country`) VALUES
(1, 'LOH', 1948, 14, 'Londýn'),
(2, 'LOH', 1952, 15, 'Helsinki'),
(3, 'LOH', 1956, 16, 'Melbourne/Štokholm'),
(4, 'LOH', 1960, 17, 'Rím'),
(5, 'LOH', 1964, 18, 'Tokio'),
(6, 'LOH', 1968, 19, 'Mexiko'),
(7, 'LOH', 1972, 20, 'Mníchov'),
(8, 'LOH', 1976, 21, 'Montreal'),
(9, 'LOH', 1980, 22, 'Moskva'),
(10, 'LOH', 1984, 23, 'Los Angeles'),
(11, 'LOH', 1988, 24, 'Soul'),
(12, 'LOH', 1992, 25, 'Barcelona'),
(13, 'LOH', 1996, 26, 'Atlanta'),
(14, 'LOH', 2000, 27, 'Sydney'),
(15, 'LOH', 2004, 28, 'Atény'),
(16, 'LOH', 2008, 29, 'Peking/Hongkong'),
(17, 'LOH', 2012, 30, 'Londýn'),
(18, 'LOH', 2016, 31, 'Rio de Janeiro'),
(19, 'LOH', 2020, 32, 'Tokio'),
(20, 'ZOH', 1964, 9, 'Innsbruck'),
(21, 'ZOH', 1968, 10, 'Grenoble'),
(22, 'ZOH', 1972, 11, 'Sapporo'),
(23, 'ZOH', 1976, 12, 'Innsbruck'),
(24, 'ZOH', 1980, 13, 'Lake Placid'),
(25, 'ZOH', 1984, 14, 'Sarajevo'),
(26, 'ZOH', 1988, 15, 'Calgary'),
(27, 'ZOH', 1992, 16, 'Albertville'),
(28, 'ZOH', 1994, 17, 'Lillehammer'),
(29, 'ZOH', 1998, 18, 'Nagano'),
(30, 'ZOH', 2002, 19, 'Salt Lake City'),
(31, 'ZOH', 2006, 20, 'Turín'),
(32, 'ZOH', 2010, 21, 'Vancouver'),
(33, 'ZOH', 2014, 22, 'Soči'),
(34, 'ZOH', 2018, 23, 'Pjongčang'),
(35, 'ZOH', 2022, 24, 'Peking');

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
CREATE TABLE IF NOT EXISTS `person` (
  `id_person` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `birthDay` varchar(10) NOT NULL,
  `birthPlace` varchar(100) NOT NULL,
  `birthCountry` varchar(100) NOT NULL,
  `deathDay` varchar(10) DEFAULT NULL,
  `deathPlace` varchar(100) DEFAULT NULL,
  `deathCountry` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_person`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`id_person`, `name`, `surname`, `birthDay`, `birthPlace`, `birthCountry`, `deathDay`, `deathPlace`, `deathCountry`) VALUES
(1, 'Peter', 'Hochschorner', '7.9.1979', 'Bratislava', 'Slovensko', '', '', '\r'),
(2, 'Pavol', 'Hochschorner', '7.9.1979', 'Bratislava', 'Slovensko', '', '', '\r'),
(3, 'Elena', 'Kaliská', '19.1.1972', 'Zvolen', 'Slovensko', '', '', '\r'),
(4, 'Anastasiya', 'Kuzmina', '28.8.1984', 'Ťumeň', 'Sovietsky zväz', '', '', '\r'),
(5, 'Michal', 'Martikán', '18.5.1979', 'Liptovský Mikuláš', 'Slovensko', '', '', '\r'),
(6, 'Ondrej', 'Nepela', '22.1.1951', 'Bratislava', 'Slovensko', '2.2.1989', 'Mannheim', 'Nemecko\r'),
(7, 'Jozef', 'Pribilinec', '6.7.1960', 'Kopernica', 'Slovensko', '', '', '\r'),
(8, 'Anton', 'Tkáč', '30.3.1951', 'Lozorno', 'Slovensko', '', '', '\r'),
(9, 'Ján', 'Zachara', '27.8.1928', 'Kubrá pri Trenčíne', 'Slovensko', '', '', '\r'),
(10, 'Július', 'Torma', '7.3.1922', 'Budapešť', 'Maďarsko', '23.10.1991', 'Praha', 'Česko\r'),
(11, 'Stanislav', 'Seman', '6.8.1952', 'Košice', 'Slovensko', '', '', '\r'),
(12, 'František', 'Kunzo', '17.9.1954', 'Spišský Hrušov', 'Slovensko', '', '', '\r'),
(13, 'Miloslav', 'Mečíř', '19.5.1964', 'Bojnice', 'Slovensko', '', '', '\r'),
(14, 'Radoslav', 'Židek', '15.10.1981', 'Žilina', 'Slovensko', '', '', '\r'),
(15, 'Pavol', 'Hurajt', '4.2.1978', 'Poprad', 'Slovensko', '', '', '\r'),
(16, 'Matej', 'Tóth', '10.2.1983', 'Nitra', 'Slovensko', '', '', '\r'),
(17, 'Matej', 'Beňuš', '2.11.1987', 'Bratislava', 'Slovensko', '', '', '\r'),
(18, 'Ladislav', 'Škantár', '11.2.1983', 'Kežmarok', 'Slovensko', '', '', '\r'),
(19, 'Peter', 'Škantár', '20.7.1982', 'Kežmarok', 'Slovensko', '', '', '\r'),
(20, 'Erik', 'Vlček', '29.12.1981', 'Komárno', 'Slovensko', '', '', '\r'),
(21, 'Juraj', 'Tarr', '18.2.1979', 'Komárno', 'Slovensko', '', '', '\r'),
(22, 'Denis', 'Myšák', '30.11.1995', 'Bojnice', 'Slovensko', '', '', '\r'),
(23, 'Tibor', 'Linka', '13.2.1995', 'Šamorín', 'Slovensko', '', '', '\r'),
(24, 'Ivan', 'Rener', '2019-03-28', 'Bratislava', 'Slovakia', '2019-03-05', 'Slovakia', 'Slovakia');

-- --------------------------------------------------------

--
-- Table structure for table `person_placed`
--

DROP TABLE IF EXISTS `person_placed`;
CREATE TABLE IF NOT EXISTS `person_placed` (
  `id` int(11) NOT NULL,
  `id_person` int(11) NOT NULL,
  `id_OH` int(11) NOT NULL,
  `place` int(11) NOT NULL,
  `discipline` varchar(100) NOT NULL,
  KEY `id_person` (`id_person`),
  KEY `id_OH` (`id_OH`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `person_placed`
--

INSERT INTO `person_placed` (`id`, `id_person`, `id_OH`, `place`, `discipline`) VALUES
(1, 1, 14, 1, 'vodný slalom - C2\r'),
(2, 1, 15, 1, 'vodný slalom - C2\r'),
(3, 1, 16, 1, 'vodný slalom - C2\r'),
(4, 1, 17, 3, 'vodný slalom - C2\r'),
(5, 2, 14, 1, 'vodný slalom - C2\r'),
(6, 2, 15, 1, 'vodný slalom - C2\r'),
(7, 2, 16, 1, 'vodný slalom - C2\r'),
(8, 2, 17, 3, 'vodný slalom - C2\r'),
(9, 3, 13, 19, 'vodný slalom - K1\r'),
(10, 3, 14, 4, 'vodný slalom - K1\r'),
(11, 3, 15, 1, 'vodný slalom - K1\r'),
(12, 3, 16, 1, 'vodný slalom - K1\r'),
(13, 4, 32, 1, 'biatlon - šprint na 7.5 km\r'),
(14, 5, 13, 1, 'vodný slalom - C1\r'),
(15, 5, 14, 2, 'vodný slalom - C1\r'),
(16, 5, 15, 2, 'vodný slalom - C1\r'),
(17, 5, 16, 1, 'vodný slalom - C1\r'),
(18, 5, 17, 3, 'vodný slalom - C1\r'),
(19, 6, 20, 22, 'krasokorčuľovanie\r'),
(20, 6, 21, 8, 'krasokorčuľovanie\r'),
(21, 6, 22, 1, 'krasokorčuľovanie\r'),
(22, 7, 11, 1, 'atletika - chôdza\r'),
(23, 8, 8, 1, 'dráhová cyklistika - šprint\r'),
(24, 9, 2, 1, 'box do 57 kg\r'),
(25, 10, 1, 1, 'box do 67 kg\r'),
(26, 11, 9, 1, 'futbal\r'),
(27, 12, 9, 1, 'futbal\r'),
(28, 13, 11, 1, 'tenis\r'),
(29, 4, 32, 2, 'biatlon - stíhacie preteky na 10 km\r'),
(30, 15, 32, 3, 'biatlon - hromadný štart\r'),
(31, 14, 31, 2, 'snoubordkros\r'),
(32, 4, 33, 1, 'biatlon - šprint na 7.5 km\r'),
(33, 4, 34, 1, 'biatlon - hromadný štart\r'),
(34, 4, 34, 2, 'biatlon - stíhacie preteky na 10 km\r'),
(35, 4, 34, 2, 'biatlon - vytrvalostné preteky na 15 km\r'),
(36, 18, 18, 1, 'vodný slalom - C2\r'),
(37, 19, 18, 1, 'vodný slalom - C2\r'),
(38, 16, 18, 1, 'atletika - chôdza\r'),
(39, 17, 18, 2, 'vodný slalom - C1\r'),
(40, 20, 18, 2, 'kanoistika - K4 na 1000m\r'),
(41, 21, 18, 2, 'kanoistika - K4 na 1000m\r'),
(42, 22, 18, 2, 'kanoistika - K4 na 1000m\r'),
(43, 23, 18, 2, 'kanoistika - K4 na 1000m\r');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `person_placed`
--
ALTER TABLE `person_placed`
  ADD CONSTRAINT `person_placed_ibfk_1` FOREIGN KEY (`id_person`) REFERENCES `person` (`id_person`),
  ADD CONSTRAINT `person_placed_ibfk_2` FOREIGN KEY (`id_OH`) REFERENCES `oh` (`id_OH`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
