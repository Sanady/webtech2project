<?php
include_once("config.php");

$conn = mysqli_connect($host, $user, $pass, $db);
mysqli_query($conn, "SET NAMES 'utf8';");

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

if(isset($_GET["id"])){
    $id = $_GET["id"];
    $fname = htmlspecialchars($_GET["Name"]);
    $lname = htmlspecialchars($_GET["Surname"]);
    $birth = htmlspecialchars($_GET["DayOfBirth"]);
    $bplace = htmlspecialchars($_GET["BirthPlace"]);
    $bcountry = htmlspecialchars($_GET["BirthCountry"]);
    $dday = htmlspecialchars($_GET["DeathDay"]);
    $dplace = htmlspecialchars($_GET["DeathPlace"]);
    $dcountry = htmlspecialchars($_GET["DeathCountry"]);

    $sql = "UPDATE `person` SET `name` = '$fname', `surname` = '$lname', `birthDay` = '$birth', `birthPlace` = '$bplace', `birthCountry` = '$bcountry', `deathDay` = '$dday', `deathPlace` = '$dplace', `deathCountry` = '$dcountry' WHERE `id_person` = '$id'";
    if (mysqli_query($conn, $sql)) {
        header('Location: index.php');
    } else {
        echo "Error updating record: " . mysqli_error($conn);
    }
}
mysqli_close($conn);

?>