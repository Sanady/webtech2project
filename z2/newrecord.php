<?php
include_once("config.php");

$conn = mysqli_connect($host, $user, $pass, $db);
mysqli_query($conn, "SET NAMES 'utf8';");

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Edit page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
  </head>
  <body>
    <div class="container">
    <br>
    <form action="newrecordadd.php" method="GET">
    <div class="form-group">
        <label for="Name">First Name:</label>
        <input type="text" class="form-control" id="Name" name="Name" placeholder="First name of olympic" required>
    </div>
    <div class="form-group">
        <label for="Surname">Last Name:</label>
        <input type="text" class="form-control" id="Surname" name="Surname" placeholder="Second name of olympic" required>
    </div>
    <div class="form-group">
        <label for="DayOfBirth">Date of birthday:</label>
        <input type="date" class="form-control" id="DayOfBirth" name="DayOfBirth" placeholder="">
    </div>
    <div class="form-group">
        <label for="BirthPlace">Birth place:</label>
        <input type="text" class="form-control" id="BirthPlace" name="BirthPlace" placeholder="Place of birth">
    </div>
    <div class="form-group">
        <label for="BirthCountry">Birth country:</label>
        <input type="text" class="form-control" id="BirthCountry" name="BirthCountry" placeholder="Country of birth">
    </div>
    <div class="form-group">
        <label for="DeathDay">Date of death:</label>
        <input type="date" class="form-control" id="DeathDay" name="DeathDay" placeholder="">
    </div>
    <div class="form-group">
        <label for="DeathPlace">Country of death:</label>
        <input type="text" class="form-control" id="DeathPlace" name="DeathPlace" placeholder="Place of death">
    </div>
    <div class="form-group">
        <label for="DeathCountry">Country of death:</label>
        <input type="text" class="form-control" id="DeathCountry" name="DeathCountry" placeholder="Country of death">
    </div>
    <button type="submit" class="btn btn-success">Submit</button>  <a href="index.php" type="button" class="btn btn-primary" >Back</a>
    </form>
    </div>

  </body>
</html>

