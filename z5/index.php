<?php
$username = 'xrener';
$password = 'Sanady-Ivan-1997';
$loginUrl = 'https://is.stuba.sk/system/login.pl';

$data = 'credential_0=' . $username . '&credential_1=' . $password . '&login=Prihlásiť sa';
$curl = curl_init();

curl_setopt($curl, CURLOPT_URL, $loginUrl);
curl_setopt($curl, CURLOPT_POST, TRUE);
curl_setopt($curl, CURLOPT_POSTFIELDS,$data);
curl_setopt($curl, CURLOPT_COOKIEJAR, 'cookie.txt');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
//execute the request (the login)

if(curl_exec($curl)==false)
{
    echo 'curl error:'.curl_error($curl);
}

$URL = 'https://is.stuba.sk/auth/katalog/rozvrhy_view.pl?rozvrh_student_obec=1?zobraz=1;format=html;lang=sk;';
curl_setopt($curl, CURLOPT_URL, $URL);
$result=curl_exec($curl);
curl_close ($curl);


$doc = new DOMDocument();
$doc->loadHTML($result);
$tables = $doc->getElementsByTagName('table');
foreach ($tables as $table) {
    $content[] = $doc->saveHTML($table);
}

$finder = new DomXPath($doc);
$classname1="rozvrh-pred";
$classname2="rozvrh-cvic";
$prednasky = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname1 ')]");
$cvicenia = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname2 ')]");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf-8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>

</head>
<body>

<?php
echo $content[0];
echo "<table class = 'table table-bordered' id='tabulka'>";
echo "<thead><tr><th>Názov Predmetu</th><th>Meno prednášateľa</th><th>Miestnosť prednášky</th><th>Meno cvičiaceho</th><th>Miestnosť cvičenia</th></tr></thead>";
for($i=0;$i<$prednasky->length;$i++) {
    $nazovp = $prednasky->item($i)->firstChild->childNodes->item(2)->nodeValue;
    $miestnostp = $prednasky->item($i)->firstChild->childNodes->item(0)->nodeValue;
    $cviciacip = $prednasky->item($i)->firstChild->childNodes->item(6)->nodeValue;
    if(strcmp($prednasky->item($i)->firstChild->childNodes->item(2)->nodeValue ,"")==0){
        $nazovp = $prednasky->item($i)->firstChild->childNodes->item(3)->nodeValue;
        $cviciacip = $prednasky->item($i)->firstChild->childNodes->item(7)->nodeValue;
    }

    for ($j = 0; $j < $cvicenia->length; $j++) {
        if (strcmp($nazovp, $cvicenia->item($j)->firstChild->childNodes->item(2)->nodeValue) == 0) {

            $miestnostc = $cvicenia->item($j)->firstChild->childNodes->item(0)->nodeValue;
            $cviciacic = $cvicenia->item($j)->firstChild->childNodes->item(4)->nodeValue;
            if(strcmp($prednasky->item($i)->firstChild->childNodes->item(2)->nodeValue ,"")==0){
                $cviciacic = $cvicenia->item($j)->firstChild->childNodes->item(6)->nodeValue;
            }
            echo "<tr><td>" . $nazovp . "</td><td>" . $cviciacip . "</td><td>" . $miestnostp . "</td><td>" . $cviciacic . "</td><td>" . $miestnostc . "</td></tr>";
        }
    }
}
echo"</table>";

?>
<script>
    $(document).ready(function () {
        $('#tabulka').DataTable({
            "searching":false,
            "info":false,
            "columns":[
                {
                    "sortable":false
                },
                {
                    "sortable":false
                },
                {
                    "sortable":false
                },
                {
                    "sortable":false
                },
                {
                    "sortable":false
                }
            ]
        });
    });
</script>

</body>

</html>
