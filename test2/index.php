<?php
session_start();
include_once("config.php");

$conn = mysqli_connect($host, $user, $pass, $db);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

?>

<!DOCTYPE html>
<html>
<html lang="sk">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Test 2</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
    <script src="main.js"></script>
</head>

<body>
<div class="container"> 
<h1>Text k ankete</h1>
<?php
if(!isset($_SESSION["hlasuj"]))
{
    echo "<br><a href='add.php' type='button' class='btn btn-primary btn-sm' >Pridaj odpoved</a><br><br>";
    $sql = "SELECT id_od, nameod FROM odpovede";
    $result = mysqli_query($conn, $sql);
    
    if (mysqli_num_rows($result) > 0) {
        echo "<table id='dbtable' class='table table-dark'><thead><tr>
        <th scope='col'>ID</th>
        <th scope='col'>Anketa</th>
        <th scope='col'>Option</th></tr></thead><tbody>";
        while($row = mysqli_fetch_assoc($result)) {
            echo "<tr><td>" . $row["id_od"] . "</td>
            <td>" . $row["nameod"] . " " . $row["id_od"] ."</td>
            <td><button type='button' class='btn btn-danger btn-sm' onClick='deleteRow(".$row["id_od"].")'>Delete</button> 
            <a href='hlasuj.php?id=". $row["id_od"] ."' type='button' class='btn btn-success btn-sm'>Hlasuj</a></td></tr>";
        }
        echo "</tbody></table>";
    } else {
        
    }
    mysqli_close($conn);
}
else
{
    header('Location: tabulka.php');   
}
/*echo "<br><a href='add.php' type='button' class='btn btn-primary btn-sm' >Pridaj odpoved</a><br><br>";
$sql = "SELECT id_od, nameod FROM odpovede";
    $result = mysqli_query($conn, $sql);
    
    if (mysqli_num_rows($result) > 0) {
        echo "<table id='dbtable' class='table table-dark'><thead><tr>
        <th scope='col'>ID</th>
        <th scope='col'>Anketa</th>
        <th scope='col'>Option</th></tr></thead><tbody>";
        while($row = mysqli_fetch_assoc($result)) {
            echo "<tr><td>" . $row["id_od"] . "</td>
            <td>" . $row["nameod"] . " " . $row["id_od"] ."</td>
            <td><button type='button' class='btn btn-danger btn-sm' onClick='deleteRow(".$row["id_od"].")'>Delete</button> 
            <a href='hlasuj.php?id=". $row["id_od"] ."' type='button' class='btn btn-success btn-sm'>Hlasuj</a></td></tr>";
        }
        echo "</tbody></table>";
    } else {
        
    }*/

?>
</div>
</body>
</html>
