<!DOCTYPE html>
<html>
<head>
    <title>Zadanie7</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

</head>
<body onload="myFunction()">

<div class="navbar">
    <a href="index.php">Weather forecast</a>
    <a href="stats.php">Stats</a>
    <a href="data.php">Info</a>
</div>

<div class="container">
    <div class="jumbotron" style="color: white; background-color: rgba(0, 0, 0, 0.8); border: 1px solid grey; border-radius: 0px; margin-top: -40px;">
        <?php
        require_once("config.php");
        include("dbinsert.php");

        $ip = getUserIP();
        //echo $ip;

        if(isset($_GET['kod'])){
            // SELECT COUNT(DISTINCT(ip)) as pocet, mesto FROM navstevy WHERE kod= 'FR' GROUP BY mesto
            echo "<h3>Detailed informations of visitor from " . $_GET['kod'] . " </h3><table class='table table-dark'>";
            echo "<thead> <tr> <th>City</th> <th>Count</th> </tr> </thhead> <tbody>";
            $podrobne = mysqli_query($conn, "SELECT COUNT(DISTINCT(ip)) as cnt, mesto FROM navstevy WHERE kod='" . $_GET['kod'] . "' GROUP BY mesto");
            while ($info = mysqli_fetch_array($podrobne)) {
                echo "<tr><td>" . $info['mesto'] . "</td> <td>" .$info['cnt'] ."</td></tr>";
            }
            echo "</tbody></table>";
        }

        //$query = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
        $query = @unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip) );

        $lat = $query['geoplugin_latitude'];
        $lon = $query['geoplugin_longitude'];

        $conn = mysqli_connect($CONF_DB_HOST, $CONF_DB_USER, $CONF_DB_PASS, $CONF_DB_NAME);
        $navstevnici = mysqli_query($conn, "SELECT COUNT(DISTINCT(ip)) as navstevnici FROM navstevy");
        $row1 = mysqli_fetch_assoc($navstevnici);

        $navstevniciStaty = mysqli_query($conn, "SELECT DISTINCT(kod), stat FROM navstevy ORDER BY kod ASC");
        echo "<h3>Visitors</h3><table class='table table-dark'>";
        echo "<thead> <tr> <th>Flag</th> <th>Country</th> <th>Count</th> </tr> </thhead> <tbody>";

        while ($rowTab = mysqli_fetch_array($navstevniciStaty)) {
            $img = strtolower($rowTab['kod']);
            $pocetJednotlive = mysqli_query($conn, "SELECT COUNT(DISTINCT(ip)) as pocet FROM navstevy WHERE kod='" . $rowTab['kod'] . "'");
            $rowJetnotlive = mysqli_fetch_assoc($pocetJednotlive);
            echo "<tr> <td style='width: 180px;'>
                      <button type='button' class='btn btn-link'><a href='?kod=" . $rowTab['kod'] . "'>
                      <img src='http://www.geonames.org/flags/l/$img.gif'> </a></button>
                      </td> <td>" . $rowTab['stat'] . "</td> <td>" . $rowJetnotlive['pocet'] . "</td> </tr>";
        }

        echo "<tr> <td> <p style='font-size: 17px;'> Count overall </p> </td> <td></td> <td> <p style='font-size: 17px;'>" . $row1['navstevnici'] . " </p></td> </tr>";
        echo "</tbody> </table>";

        $markers = mysqli_query($conn, "SELECT DISTINCT(poloha) FROM navstevy");
        ?>

        <?php
        $noc = mysqli_query($conn, "SELECT count(distinct(ip)) as v FROM navstevy WHERE HOUR(datum) >= 0 AND HOUR(datum) <= 6");
        $rano = mysqli_query($conn, "SELECT count(distinct(ip)) as v FROM navstevy WHERE HOUR(datum) > 6 AND HOUR(datum) <= 14");
        $obed = mysqli_query($conn, "SELECT count(distinct(ip)) as v FROM navstevy WHERE HOUR(datum) > 14 AND HOUR(datum) <= 20");
        $vecer = mysqli_query($conn, "SELECT count(distinct(ip)) as v FROM navstevy WHERE HOUR(datum) > 20 AND HOUR(datum) < 24");

        echo "<br><br><table class='table'> <thead> <tr> <th>Visit time</th> <th>Visit count</th> </tr> </thead> <tbody>";
        echo "<tr><td>0:00 - 6:00</td> <td>".mysqli_fetch_assoc($noc)['v']."</td></tr>";
        echo "<tr><td>6:00 - 14:00</td> <td>".mysqli_fetch_assoc($rano)['v']."</td></tr>";
        echo "<tr><td>14:00 - 20:00</td> <td>".mysqli_fetch_assoc($obed)['v']."</td></tr>";
        echo "<tr><td>20:00 - 0:00</td> <td>".mysqli_fetch_assoc($vecer)['v']."</td></tr></tbody></table>";


        $najviacNavstevovana = mysqli_query($conn, "SELECT stranka, COUNT(*) as pocet FROM navstevy GROUP BY stranka ORDER BY pocet DESC LIMIT 1");
        $row2 = mysqli_fetch_assoc($najviacNavstevovana);
        if($row2['stranka'] == "stats.php"){
            $stranka = "Statistics";
        }
        else if($row2['stranka'] == "pocasie.php"){
            $stranka = "Weather";
        }
        else {
            $stranka = "Informations";
        }
        echo "<h3>The most visited page: " . $stranka . "</h3>";
        ?>
    </div>
</div>
<script src="script.js"></script>
<footer>
    <small>&copy; Copyright <span id="footer"></span>, Ivan Rener</small>

</footer>
</body>
</html>

