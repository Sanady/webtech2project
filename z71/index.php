<!DOCTYPE html>
<html>
<head>
<title>Zadanie7</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="weather.css">
    <script src="script.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

</head>
<body onload="myFunction()">

<div class="navbar">
    <a href="index.php">Weather forecast</a>
    <a href="stats.php">Stats</a>
    <a href="data.php">Info</a>
</div>

<div class="container">
    <?php


    include("dbinsert.php");
    //include("getUserIp.php");

    $ip = getUserIP();



    //echo $ip; // Output IP address [Ex: 177.87.193.134]


    //$query = @unserialize(file_get_contents('http://ip-api.com/php/'. $ip));

    $apiKey = "d49ba2cdbda030176685ed031d393603";



    //$query = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
    $query = @unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip) );

    //if ($query && $query['status'] == 'success'){
        //get Coords
        $lat = $query['geoplugin_latitude'];
        $lon = $query['geoplugin_longitude'];
        $city = $query['geoplugin_city'];
        $code = $query['geoplugin_countryCode'];

        $apikey = 'd49ba2cdbda030176685ed031d393603';
        $url  = "http://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$lon&APPID=$apikey&units=metric";
        $url2 = "http://api.openweathermap.org/data/2.5/forecast?lat=$lat&lon=$lon&cnt=7&APPID=$apikey&units=metric";
        // $url3 = "http://api.openweathermap.org/data/2.5/forecast/daily?lat=$lat&lon=$lon&APPID=$apikey&units=metric&cnt=7";
        $weather = file_get_contents($url);
        $forecast = file_get_contents($url2);
        // echo $forecast . "<br>";
        $json = json_decode($weather, true);
        $json2 = json_decode($forecast, true);



         //echo var_dump($json2);
    echo "

<div class='current'><h2>Current weather</h2></div><hr>";
        ?>

        <div class='box'>

        <?php echo "<h1>". $json["main"]["temp"] . "°C</h1><img src='https://www.travangelo.co.uk/images/weather-icons/" . $json["weather"][0]["icon"] . ".png' width='18%' height='18%'>" ."
        ".$city.", ".$code ." <h3>" . $json["weather"][0]["description"] . "</h3></div>";
        ?>
        <div class='box'>

        <table class="table table-hover"><tr>
                    <th><h3>Sunsire</h3></th>
                    <th><h3>Sunset</h3></th>
                    </tr>
                <tr>
                <td><h2>
                    <?php
                $timezone  = 2; //(GMT 2:00)
                echo gmdate("H:i ", $json["sys"]["sunrise"] + 3600*($timezone+date("I")))."</h2></td>";

                echo "
                <td><h2>". gmdate("H:i ", $json["sys"]["sunset"] + 3600*($timezone+date("I")))."</h2></td></tr>";
        //echo  date('H:i',$json["sys"]["sunrise"]) ;
        echo "</table>";
        ?>
        </div>
            <?php
        echo "<table class=\"table table-hover\">
                    <thead>  
                    <tr>
                    <th>Time</th>
                    <th>Temp [°C]</th>
                    <th>Clouds [%]</th>
                     <th>Pressure [hPa]</th>
                      <th>Humidity [%]</th>
                      <th>Min temp [°C]</th>
                      <th>Max temp [°C]</th>
                      <th>Wind [m/s]</th>
                      <th>Rain [mm]</th>
                      
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    <td>".date('F j, Y, g:i a',$json["dt"])."</td>
                    <td>" . $json["main"]["temp"] . "</td>
                     <td>" . $json["clouds"]["all"] . "</td> 
                    <td>" . $json["main"]["pressure"] . "</td>
                    <td>" . $json["main"]["humidity"] . "</td>
                    <td>" . $json["main"]["temp_min"] . "</td>
                    <td>" . $json["main"]["temp_max"] . "</td>
                    <td>" . $json["wind"]["speed"] . "</td>
                    <td>" . $json["rain"]["1h"] . "</td>
                   
                    </tr></tbody></table>
                    ";

        echo "<hr>";

        //echo date('Y-m-d h:i:s',$json2["list"][6]["dt"]);

    //echo "console.log(timeConverter($json2.[\"list\"][6][\"dt_txt\"]))";
        echo "<h2>Weather forecast </h2>";
        echo "<table class=\"table table-hover\">
                    <thead>  
                    <tr>
                    <th>Time</th>
                    <th>Temp [°C]</th>
                    <th></th>
                    <th>Clouds [%]</th>
                     <th>Pressure [hPa]</th>
                      <th>Humidity [%]</th>
                      <th>Min temp [°C]</th>
                      <th>Max temp [°C]</th>
                      <th>Wind [m/s]</th>
                      <th>Rain [mm]</th>
                      
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>";
                    for($i=0; $i<=6; $i++){

                    echo "<tr>
                    <td>".date('F j, Y, g:i a',$json2["list"][$i]["dt"])."</td>
                    <td>" . $json2["list"][$i]["main"]["temp"] . "</td>
                    <td align='left'><img src='https://www.travangelo.co.uk/images/weather-icons/" . $json2["list"][$i]["weather"][0]["icon"] .".png' width='40%' height='40%'>
                    " . $json2["list"][$i]["weather"][0]["description"] ."
                    </td>
                    <td>" . $json2["list"][$i]["clouds"]["all"] ."</td>
                    <td>" . $json2["list"][$i]["main"]["pressure"] . "</td>
                    <td>" . $json2["list"][$i]["main"]["humidity"] . "</td>
                    <td>" . $json2["list"][$i]["main"]["temp_min"] ."</td>
                    <td>" . $json2["list"][$i]["main"]["temp_max"] ."</td>
                    <td>" . $json2["list"][$i]["wind"]["speed"] ."</td> 
                    <td>" . $json2["list"][$i]["rain"]["3h"] ."</td> 

                    </tr>";
                    }
                    echo "</tbody></table>";


    ?>
</div>
<footer>
    <small>&copy; Copyright <span id="footer"></span>, Ivan Rener</small>

</footer>
</body>
</html>
