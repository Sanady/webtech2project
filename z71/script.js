function myFunction() {

    var d = new Date();

    currentYear = d.getFullYear();

    document.getElementById("footer").innerHTML = currentYear;

}

function confirmMessage() {
    var txt;
    var test;
    var r = confirm("Suhlasis so spracovanim tvojich udajov ako je IP adresa alebo GPS suradnice?");
    if (r == true) {
        txt = "You pressed OK";
        test = true;
    } else {
        txt = "You pressed Cancel!";
        test = false;

    }
    return test;
}

function start() {
    confirmMessage();
    myFunction();
}