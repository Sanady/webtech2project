<!DOCTYPE html>
<html>
<head>
    <title>Zadanie7</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body onload="myFunction()">

<div class="navbar">
    <a href="index.php">Weather forecast</a>
    <a href="stats.php">Stats</a>
    <a href="data.php">Info</a>
</div>

<div class="container">
<?php

include("dbinsert.php");
//include("getUserIp.php");

$ip = getUserIP();

$query = @unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip) );

//echo $query;

//if ($query && $query['status'] == 'success'){
    $lat = $query['geoplugin_latitude'];
    $lon = $query['geoplugin_longitude'];
    $apikey = 'd49ba2cdbda030176685ed031d393603';
    $url = "http://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$lon&APPID=$apikey&units=metric";
    $weather = file_get_contents($url);
    $json = json_decode($weather, true);

echo "<div class='current'><h2>Your information</h2></div><hr>";
    echo "<table class=\"table table-hover\">
<thead>  
<tr>
<th>IP address</th>
<th>GPS coords</th>
<th>City</th>
<th>Country</th>
<th>Capital city</th>
</tr>
</thead>  ";
    echo "<tbody><tr><td>" .$ip . "</td>";
    echo "<td>" .$lat . ", " . $lon . "</td>";

    if($query['geoplugin_city'] == null){
        $city = "nedá sa lokalizovať alebo sa nachádzate na vidieku";
    }
    else{
        $city = $query['geoplugin_city'];
    }
    $country = $query['geoplugin_countryName'];
    echo "<td>" . $city . "</td> ";
    echo "<td>" . $country . "</td>";

    $hlavne = json_decode(file_get_contents("https://restcountries.eu/rest/v2/name/".rawurlencode($country)))[0]->capital;
    echo "<td>$hlavne</td></tr></tbody></table> ";
/*
}
else{
    echo "error";
}
*/
$query = mysqli_query($conn,"SELECT DISTINCT(poloha),mesto FROM navstevy");

$gps = array();
while($row = mysqli_fetch_array($query)){
    $gps[] = $row['poloha'];
    $mesto[] = $row['mesto'];
}

?>
    <h3>Google map of visitors</h3>
    <div id="googleMap" style="width:100%;height:450px;margin-bottom: 50px"></div>

    <script type="application/javascript">
        var lat = <?php echo $lat ?>;
        var lon = <?php echo $lon ?>;

        var a = <?php echo json_encode($gps); ?>;
        var mesto = <?php echo json_encode($mesto); ?>;
        //window.alert(mesto);
        function myMap() {
            var mapProp = {

                center: new google.maps.LatLng(lat,lon),
                zoom: 3,
            };
            var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

            var i;
            for (i = 0; i < a.length; i++) {

            var split = a[i].split(",");

            new google.maps.Marker({
                position: new google.maps.LatLng(split[0],split[1]),
                map: map,
                title: mesto[i],
            });

                var infowindow = new google.maps.InfoWindow({
                    content: "seva"
                });


            }
        }
</script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQ9QsiACOvER2tZHwFYOoW2E2dHiRmVXg&callback=myMap"></script>



</div>
<script src="script.js"></script>
<footer>
    <small>&copy; Copyright <span id="footer"></span>, Ivan Rener</small>

</footer>
</body>
</html>
