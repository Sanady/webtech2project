<?php
header('Content-Type: application/json');
$XMLko = new DOMDocument();
$XMLko->load('meniny.xml');

function najdi_menoPodlaDatumu($datum, $krajina='SK') {
    global $XMLko;
    $vsetky_dni = $XMLko->getElementsByTagName('zaznam');
    foreach($vsetky_dni as $den) {
        if($den->getElementsByTagName('den')->item(0)->nodeValue == $datum){
            return ['name' => $den->getElementsByTagName($krajina)->item(0)->nodeValue];
        }
    }
    return null;
}
function najdi_datumPodlaMena($meno, $krajina='SK') {
    global $XMLko;
    $vsetky_dni = $XMLko->getElementsByTagName('zaznam');
    foreach($vsetky_dni as $den) {
        if($den->getElementsByTagName($krajina)->item(0)->nodeValue == $meno){
            return ['date' => $den->getElementsByTagName('den')->item(0)->nodeValue];
        }
    }
    return null;
}
function najdi_dniPodlaTagName($tag) {
    global $XMLko;
    $pole_dni = array();
    $vsetky_dni = $XMLko->getElementsByTagName($tag);
    foreach($vsetky_dni as $den) {
        $date = $den->parentNode->getElementsByTagName('den')->item(0);
        $item = ['date' => $date->nodeValue, 'title' => $den->nodeValue];
        array_push($pole_dni, $item);
    }
    return $pole_dni;
}
function vlozDalsieMeno($meno, $datum) {
    global $XMLko;
    $vsetky_dni = $XMLko->getElementsByTagName('den');
    foreach($vsetky_dni as $den) {
        if ($den->nodeValue == $datum) {
            $target = $den->parentNode->getElementsByTagName('SKd')->item(0);
            $vsetky_mena= $target->nodeValue.", ".$meno;
            $target->nodeValue = $vsetky_mena;
            return $XMLko->save('meniny.xml') != false;
        }
    }
    return false;
}
function response($status,$status_message,$data){
    header("HTTP/1.1 ".$status);

    $odpoved['status']=$status;
    $odpoved['status_message']=$status_message;
    $odpoved['data']=$data;

    $json_odpoved = json_encode($odpoved);
    echo $json_odpoved;
}


$method = $_SERVER['REQUEST_METHOD'];
$request = explode('/', trim($_SERVER['PATH_INFO'],'/'));


if($method == 'GET') {
    if (!empty($request[0])) {
        $func = array_shift($request);
        if (empty($request)) {
            if ($func == 'sk') {
                $odpoved = najdi_dniPodlaTagName('SKsviatky');
                $status = 200;
                $statusMsg = 'OK';
            } elseif ($func == 'cz') {
                $odpoved = najdi_dniPodlaTagName('CZsviatky');
                $status = 200;
                $statusMsg = 'OK';
            } elseif ($func == 'skdni') {
                $odpoved = najdi_dniPodlaTagName('SKdni');
                $status = 200;
                $statusMsg = 'OK';
            } else {
                $status = 400;
                $statusMsg = 'Bad request';
            }
        } else {
            if(sizeof($request) == 2) {
                $country = array_shift($request);
                $arg = array_shift($request);

                if($func == 'sviatky') {
                    if(is_numeric($arg)) {
                        $odpoved = najdi_menoPodlaDatumu($arg, strtoupper($country));
                        $status = 200;
                        $statusMsg = 'OK';
                    }
                    else {
                        $odpoved = najdi_datumPodlaMena($arg, strtoupper($country));
                        $status = 200;
                        $statusMsg = 'OK';
                    }
                    if($odpoved == null) $statusMsg = 'Not found';
                }
            }
            else {
                $status = 400;
                $statusMsg = 'Bad request';
            }
        }
    }
    else {
        $status = 400;
        $statusMsg = 'Bad request';
    }
}
elseif($method == 'POST') {
    if(sizeof($request) == 1) {
        if(array_shift($request) == 'skd') {
            if(isset($_POST['date']) && isset($_POST['name'])) {
                $date = $_POST['date'];
                $name = $_POST['name'];
                if (is_numeric($date)) {
                    if (vlozDalsieMeno($name, $date)) {
                        $status = 201;
                        $statusMsg = 'Created';
                    } else {
                        $status = 409;
                        $statusMsg = 'Conflict';
                    }
                } else {
                    $status = 400;
                    $statusMsg = 'Bad request';
                }
            }
            else {
                $status = 400;
                $statusMsg = 'Bad request';
            }
        }
        else {
            $status = 400;
            $statusMsg = 'Bad request';
        }
    }
    else {
        $status = 400;
        $statusMsg = 'Bad request';
    }
}
response($status, $statusMsg, $odpoved);