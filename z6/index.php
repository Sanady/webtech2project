<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="UTF-8">
    <title>Zadanie 6</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="main.js"></script>
</head>

<body>
<div class="jumbotron text-center">
    <h2>Zadanie 6. - REST </h2>
</div>
<div class="container">
    <form>
        <label for="akcia">Akcia  </label>
        <select id="akcia">
            <option>Slovenské sviatky</option>
            <option>České sviatky</option>
            <option>Slovenské pamätné dni</option>
            <option>Kto má meniny?</option>
            <option>Kedy má meniny?</option>
            <option>Vložiť meno k dátumu</option>
        </select>
        <label for="krajina" id="napis_Krajina">Krajina  </label>
        <select id="krajina">
            <option>SK</option>
            <option>CZ</option>
            <option>HU</option>
            <option>PL</option>
            <option>AT</option>
        </select>
        <label for="form_Meno" id="napis_Meno">Meno  </label>
        <input type="text" id="form_Meno" placeholder="meno">
        <label for="date" id="dateLab">Dátum  </label>
        <input type="date" id="date">
        <input class="btn btn-success" id="btn" type="button" value="Vykonať"/>
    </form>
</div>
<div class="container">
    <h3>Výsledok</h3>
    <div id="output"></div>
    <div id="description">
        <br>
        <h3>REST API dokumentácia</h3>
        <b>Slovenské sviatky</b> - Tento endpoint vráti všetky Slovenské sviatky registrované v našom XML<br/>
        <b>GET</b> http://147.175.121.210:8118/z6/server.php/sk<br/><br/>
        <b>České sviatky</b> - Tento endpoint vráti všetky České sviatky registrované v našom XML<br/>
        <b>GET</b> http://147.175.121.210:8118/z6/server.php/cz<br/><br/>
        <b>Slovenské pamätné dni</b> - Tento endpoint vráti všetky Slovenské pamätné dni registrované v našom XML<br/>
        <b>GET</b> http://147.175.121.210:8118/z6/server.php/skdni<br/><br/>
        <b>Kto má meniny</b> - Tento endpoint vyhľadá a vráti meno v kalendári na základe vloženého dátumu a krajiny.<br/>
        <b>Kedy má meniny</b> - Tento endpoint vyhľadá a vráti dátum menín v zvolenej krajine pre vložené meno v kalendári.<br/>
        <b>GET</b> http://147.175.121.210:8118/z6/server.php/sviatky/<b>{kód krajiny}</b>/<b>{dátum v tvare mmdd / meno}</b><br/><br/>
        <b>Vložiť meno k dátumu</b> - Skrz tento endpoint je možné vložiť meno do nášho XML kalendára.<br/>
        <b>POST</b> http://147.175.121.210:8118/z6/server.php/skd/<br/>
        <b>Body atribúty:</b>
        <ul>
            <li>date - dátum v tvare mmdd</li>
            <li>name - meno, ktoré chceme na daný dátum vložiť</li>
        </ul>
    </div>
</div>
<div class="jumbotron text-center" style="margin-bottom:0">
    <p>Ivan Rener. Web-tech 2, zadanie 6.</p>
</div>
</body>
</html>
