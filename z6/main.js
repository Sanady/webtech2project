$(function(){
    var select =  $("#akcia");

    select.change(function() {

        if(select.val() === "Kedy má meniny?") {
            $("#napis_Krajina").show();
            $("#krajina").show();
            $("#form_Meno").show();
            $("#napis_Meno").show();
            $("#dateLab").hide();
            $("#date").hide();
        }
        else if(select.val() === "Kto má meniny?") {
            $("#napis_Krajina").show();
            $("#krajina").show();
            $("#form_Meno").hide();
            $("#napis_Meno").hide();
            $("#dateLab").show();
            $("#date").show();
        }
        else if(select.val() === "Vložiť meno k dátumu") {
            $("#form_Meno").show();
            $("#napis_Meno").show();
            $("#dateLab").show();
            $("#date").show();
            $("#krajina").hide();
            $("#napis_Krajina").hide();
        }
        else {
            $("#krajina").hide();
            $("#form_Meno").hide();
            $("#napis_Krajina").hide();
            $("#napis_Meno").hide();
            $("#dateLab").hide();
            $("#date").hide();
        }
    });
    $("#btn").click(function(){
        if(select.val() === "Slovenské sviatky") {
            $.ajax({
                        type: 'GET',
                        url: 'http://147.175.121.210:8118/z6/server.php/sk',
                        success: function(msg){
                if(msg.status == 200) {
                    var output = "";
                    for (var i = 0; i < msg.data.length; i++) {
                        output += msg.data[i].date.substr(2) + "." + msg.data[i].date.substr(0, 2) + " - " + msg.data[i].title + "<br>";
                    }
                                $("#output").html(output);
                            }
                else $("#output").html(msg.status + " " +msg.status_message);
            }
                    });
                }
        else if(select.val() === "České sviatky") {
            $.ajax({
                        type: 'GET',
                        url: 'http://147.175.121.210:8118/z6/server.php/cz',
                        success: function(msg){
                if(msg.status == 200) {
                    if(msg.status_message === "Not found") $("#output").html("Žiadne výsledky");
                    else {
                        var output = "";
                        for (var i = 0; i < msg.data.length; i++) {
                            output += msg.data[i].date.substr(2) + "." + msg.data[i].date.substr(0, 2) + " - " + msg.data[i].title + "<br>";
                        }
                                    $("#output").html(output);
                                }
                }
                else $("#output").html(msg.status + " " +msg.status_message);
            }
                    });
                }
        else if(select.val() === "Slovenské pamätné dni") {
            $.ajax({
                        type: 'GET',
                        url: 'http://147.175.121.210:8118/z6/server.php/skdni',
                        success: function(msg){
                if(msg.status == 200) {
                    if(msg.status_message === "Not found") $("#output").html("Žiadne výsledky");
                    else {
                        var output = "";
                        for (var i = 0; i < msg.data.length; i++) {
                            output += msg.data[i].date.substr(2) + "." + msg.data[i].date.substr(0, 2) + " - " + msg.data[i].title + "<br>";
                        }
                                    $("#output").html(output);
                                }
                }
                else $("#output").html(msg.status + " " +msg.status_message);
            }
                    });
                }
        else if(select.val() === "Kto má meniny?") {
            var date = $("#date").val();
            date = date.substr(5,2) + date.substr(8,2);
            var cntryCode = $("#krajina").val();
            $.ajax({
                        type: 'GET',
                        url: 'http://147.175.121.210:8118/z6/server.php/sviatky/'+cntryCode+"/"+date,
                        success: function(msg){
                if(msg.status == 200) {
                    if(msg.status_message === "Not found") $("#output").html("Žiadne výsledky");
                    else {
                        $("#output").html(msg.data.name);
                    }
                }
                else $("#output").html(msg.status + " " +msg.status_message);
            }
                    });
                }
        else if(select.val() === "Kedy má meniny?") {
            var name = $("#form_Meno").val();
            var cntryCode = $("#krajina").val();
            $.ajax({
                        type: 'GET',
                        url: 'http://147.175.121.210:8118/z6/server.php/sviatky/'+cntryCode+"/"+name,
                        success: function(msg){
                if(msg.status == 200) {
                    if(msg.status_message === "Not found") $("#output").html("Žiadne výsledky");
                    else {
                        $("#output").html(msg.data.date.substr(2) + "." + msg.data.date.substr(0, 2));
                    }
                }
                else $("#output").html(msg.status + " " +msg.status_message);
            }
                    });
                }
        else if(select.val() === "Vložiť meno k dátumu") {
            var name = $("#form_Meno").val();
            var date = $("#date").val();
            date = date.substr(5,2) + date.substr(8,2);

            var url = 'http://147.175.121.210:8118/z6/server.php/skd/';
            var data = { name: name, date: date };
            console.log(data);
            $.post(url, data)
                .done(function(msg) {
                    console.log(msg);
                    if(msg.status === 201)
                        $("#output").html("Meno bolo vložené");
                    else
                        $("#output").html("Meno sa nepodarilo vložiť!");
                })
                .fail(function() {
                    $("#output").html("Meno sa nepodarilo vložiť!");
                })
        }
    });
});